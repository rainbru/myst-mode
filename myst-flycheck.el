(flycheck-define-checker myst-build
  "A Myst syntax checker using myst build"
  :command ("myst"
            "build"
            "--no-build"
            "--no-color"
            source-inplace)
  :error-patterns
  ((error line-start "Error in " (file-name) ":" line ":" (message) line-end)
   (error line-start "Syntax error in " (file-name) ":" line ":" (message) line-end)
   (warning line-start "Warning in " (file-name) ":" line ":" (message) line-end)
   )
  :modes myst-mode
  )

(add-to-list 'flycheck-checkers 'myst-build)
