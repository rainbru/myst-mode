# myst-mode

A minimal [myst](https://github.com/myst-lang/myst) mode for emacs, 
based on [crystal-mode](https://github.com/jpellerin/emacs-crystal-mode).

# Installation

- Clone the project or download it
- Move it to a subdir of `/usr/share/emacs/site-lisp/`

If you wan't to install it as a *symlink* :

```
cd /usr/share/emacs/site-lisp/
sudo ln -s /path-to-real-directory-of/myst-mode myst-mode
```

- Add this to the `~/.emacs.d/init.el` file

```elisp
(load-file "~/.emacs.d/myst-mode/myst-mode.el")
(add-to-list 'auto-mode-alist '("\\.mt$" . myst-mode))
(add-to-list 'interpreter-mode-alist '("myst" . myst-mode))
```

# Changes from crystal-mode

The `deftype` keyword was added both to font-locking and indentation rules.

# Standard tasks

## Adding a keyword

To provide both font-locking and indentation, the keyword must be
added to the following lists : `myst-block-beg-keywords`, `myst-indent-beg-re`,
`myst-defun-beg-re`, `myst-smie-grammar`, `myst-smie-rules`, 
`myst-font-lock-keywords`.
