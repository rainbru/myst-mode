# Should correctly handle lists. Here is a compilation of code
# using lists found in the myst's examples directory

list = []

[nil, "hello", nil].each() do |elem|
  when elem == nil
    next
  end

  STDOUT.puts(elem)
end

i = 0
result = [1, 2, nil, 4, 5].each() do |elem|
  when elem == nil
    break :err
  end
  i = i + 1
end

memory = [1, 1]
n = 200
x = 2
while x <= n
  memory[x] = memory[x-1] + memory[x-2]
  x = x + 1
end

