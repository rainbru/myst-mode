# The following function is a contrived example that should really be handled
# with pattern matching for the arguments, but shows how `return` can be used
# to exit a function early.
list = []
def add_if_not_nil(element)
  unless element
    return nil
  end

  list = list + [element]
end

STDOUT.puts(add_if_not_nil(1))   #=> [1]
STDOUT.puts(add_if_not_nil(nil)) #=> nil
# Because of the early return when called with `nil`, `list` should only
# contain the `1` that was added first.
STDOUT.puts(list) #=> [1]

# `break` will return from the function the block was passed to. If a value is
# given to the `break`, it will be used as the return value from the function.
# If no value is given, the result will be `nil`.
i = 0
result = [1, 2, nil, 4, 5].each() do |elem|
  when elem == nil
    break :err
  end
  i = i + 1
end

STDOUT.puts(result) #=> :err
# Because `break` caused `each` to exit before iterating all of the elements,
# `i` was only incremented twice.
STDOUT.puts("Iterations: " + i.to_s()) #=> 2

