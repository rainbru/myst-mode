# Should consider deftype as a keywork

deftype List
  def contains(element)
    each(&fn
      ->(<element>) { break true }
      ->(_)         { false }
    end)
  end
end
