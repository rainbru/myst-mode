;; To run unit tests
;; `emacs -batch -l ert -l myst-test.el -f ert-run-tests-batch-and-exit`

(ert-deftest addition-test ()
  (should (= (+ 1 3) 4)))
